# README #

Implement from various machine learning model and statistics tools from scratch, and apply them for digit recognition of MNIST data set.

### Included Model ###

* Linear Regression
* SVM
* Multinomial (Softmax) Regression
* SVM with Kernel
* PCA
* Neural Network MLP
* Convolution Neural Network

### Folder #####
* Dataset: dataset used
* part1: written-from-scratch basic ML algorithms, including: Linear regression, Softmax regression, Gradient Descent Algor, PCA, Cubic Kernel.
* part2-nn: written-from-scratch MLP neural network with gradient descent.
* part2-mnist: Pytorch based fully-connected NN and convolution NN
* part2-twodigit: Same as previous this time trainning to recognize two overlapping digits.
